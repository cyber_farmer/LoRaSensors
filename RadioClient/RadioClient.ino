#include <SPI.h>
#include <RH_RF95.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
#include "Adafruit_Si7021.h"
#include <avr/dtostrf.h>
#include <ArduinoUniqueID.h>

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#if defined(ESP8266)
  /* for ESP w/featherwing */ 
  #define RFM95_CS  2    // "E"
  #define RFM95_RST 16   // "D"
  #define RFM95_INT 15   // "B"

#elif defined(ESP32)  
  /* ESP32 feather w/wing */
  #define RFM95_RST     27   // "A"
  #define RFM95_CS      33   // "B"
  #define RFM95_INT     12   //  next to A

#elif defined(NRF52)  
  /* nRF52832 feather w/wing */
  #define RFM95_RST     7   // "A"
  #define RFM95_CS      11   // "B"
  #define RFM95_INT     31   // "C"
  
#elif defined(TEENSYDUINO)
  /* Teensy 3.x w/wing */
  #define RFM95_RST     9   // "A"
  #define RFM95_CS      10   // "B"
  #define RFM95_INT     4    // "C"
#endif

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

Adafruit_Si7021 sensor = Adafruit_Si7021();

Adafruit_SH110X display = Adafruit_SH110X(64, 128, &Wire);

// OLED FeatherWing buttons map to different pins depending on board:
#if defined(ESP8266)
  #define BUTTON_A  0
  #define BUTTON_B 16
  #define BUTTON_C  2
#elif defined(ESP32)
  #define BUTTON_A 15
  #define BUTTON_B 32
  #define BUTTON_C 14
#elif defined(ARDUINO_STM32_FEATHER)
  #define BUTTON_A PA15
  #define BUTTON_B PC7
  #define BUTTON_C PC5
#elif defined(TEENSYDUINO)
  #define BUTTON_A  4
  #define BUTTON_B  3
  #define BUTTON_C  8
#elif defined(ARDUINO_NRF52832_FEATHER)
  #define BUTTON_A 31
  #define BUTTON_B 30
  #define BUTTON_C 27
#else // 32u4, M0, M4, nrf52840 and 328p
  #define BUTTON_A  9
  #define BUTTON_B  6
  #define BUTTON_C  5
#endif

unsigned long minutes = 5 * 60 * 1000UL;
static unsigned long lastSampleTime = 0 - minutes;

// Main Menu
void menu() {
  display.setCursor(0,0);
  display.clearDisplay();
  delay(500);

  display.println("      Main Menu\n");
  display.println(" A: Show Temperature\n");
  display.println(" B: Send Sensor Data\n");
  display.println(" C: Configure System\n");
  display.display();
  while (true) {
    if(!digitalRead(BUTTON_A)) { 
      printData();
      break;
    }
    if(!digitalRead(BUTTON_B)) {
      sendData();
      break;
    }
    if(!digitalRead(BUTTON_C)) {
      configure();
      break;
    }
  }
}

// Config Menu
void configure() {
  display.setCursor(0,0);
  display.clearDisplay();
  delay(500);

  display.println("  Configuration Menu\n");
  display.println(" A: Edit Data Rate\n");
  display.println(" B: ID Configuration\n");
  display.println(" C: Cancel\n");
  display.display();
  while (true) {
    if(!digitalRead(BUTTON_A)) { 
      msgRate();
      break;
    }
    if(!digitalRead(BUTTON_B)) {
      configID();
      break;
    }
    if(!digitalRead(BUTTON_C)) {
      delay(500);
      break;
    }
  }
}

// Message Rate
void msgRate() {
  display.setCursor(0,0);
  display.clearDisplay();
  delay(500);

  display.println("   Data Send Rate\n");
  display.println(" A: 1 Minute\n");
  display.println(" B: 5 Minutes Default\n");
  display.println(" C: 15 Minutes\n");
  display.display();
  while (true) {
    if(!digitalRead(BUTTON_A)) { 
      minutes = 1 * 60 * 1000UL;
      display.setCursor(0,0);
      display.clearDisplay();
      display.println("   Data Send Rate\n");
      display.println(" Set to 1 minute");
      display.display();
      delay(5000);
      break;
    }
    if(!digitalRead(BUTTON_B)) {
      minutes = 5 * 60 * 1000UL;
      display.setCursor(0,0);
      display.clearDisplay();
      display.println("   Data Send Rate\n");
      display.println(" Set to 5 minutes");
      display.display();
      delay(5000);
      break;
    }
    if(!digitalRead(BUTTON_C)) {
      minutes = 15 * 60 * 1000UL;
      display.setCursor(0,0);
      display.clearDisplay();
      display.println("   Data Send Rate\n");
      display.println(" Set to 15 minutes");
      display.display();
      delay(5000);
      break;
    }
  }
}

// Unique ID "configuration"
void configID() {
  display.setCursor(0,0);
  display.clearDisplay();
  delay(500);

  display.println("   Device ID Config\n");
  display.println(" A: Show ID\n");
  display.println(" B: Send ID\n");
  display.println(" C: Cancel\n");
  display.display();
  while (true) {
    if(!digitalRead(BUTTON_A)) { 
      display.setCursor(0,0);
      display.clearDisplay();
      display.println("       Device ID\n");
      UniqueIDdump(Serial);
      for (size_t i = 0; i < UniqueIDsize; i++)
      {
        if (UniqueID[i] < 0x10)
        display.print("0");
        display.print(UniqueID[i], HEX);
        display.print(" ");
      }
      display.display();
      delay(10000);
      break;
    }
    if(!digitalRead(BUTTON_B)) {
      sendData();
      break;
    }
    if(!digitalRead(BUTTON_C)) {
      break;
    }
  }
}


// Prints sensor data to OLED
void printData() {
  display.setCursor(0,0);
  display.clearDisplay();

  display.println("     Current H/T\n");
  
  display.print(" Humidity: ");
  display.print(sensor.readHumidity(), 2);
  display.println("%\n");
  
  display.print(" Temperature: ");
  float temp = sensor.readTemperature();
  temp = (temp*1.8)+32;
  display.print(temp);
  display.println("F");
  
  display.display();
  delay(10000);
}

// Function to send data
void sendData() {
  display.setCursor(0,0);
  display.clearDisplay();
  
  // Prepping variables
  char packetID[32];
  char packetH[5];
  char packetT[5];
  char packet[45];
  float temp = sensor.readTemperature();
  float humid = sensor.readHumidity();
  
  UniqueIDdump(Serial);
  String ID = String("");
  for (size_t i = 0; i < UniqueIDsize; i++)
  {
    if (UniqueID[i] < 0x10)
      ID += ("0");
      ID += String(UniqueID[i], HEX);
  }

  // Converting temp from C to F
  temp = (temp*1.8)+32;

  // Putting all data in a char array, packet
  ID.toCharArray(packetID, 32);
  dtostrf(humid, 5, 2, packetH);
  dtostrf(temp, 5, 2, packetT);
  strcpy(packet, packetID);
  strcat(packet, " ");
  strcat(packet, packetH);
  strcat(packet, " ");
  strcat(packet, packetT);
  strcat(packet, " ");

  // Actual packet sending
  rf95.send((uint8_t *)packet, 45);

  // Status messages
  display.println(" Sending");
  display.print(" ID: ");
  display.println(packetID);
  display.print(" Humidity: ");
  display.println(packetH);
  display.print(" Temperature: ");
  display.println(packetT);
  display.display(); 
  delay(10);

  // Confirm data was sent successfully
  rf95.waitPacketSent();
  sendConfirm();
}

// Function to send data without displaying it
void sendDataQuiet() {
  // Prepping variables
  char packetID[32];
  char packetH[5];
  char packetT[5];
  char packet[45];
  float temp = sensor.readTemperature();
  float humid = sensor.readHumidity();
  
  UniqueIDdump(Serial);
  String ID = String("");
  for (size_t i = 0; i < UniqueIDsize; i++)
  {
    if (UniqueID[i] < 0x10)
      ID += ("0");
      ID += String(UniqueID[i], HEX);
  }

  // Converting temp from C to F
  temp = (temp*1.8)+32;

  // Putting all data in a char array, packet
  ID.toCharArray(packetID, 32);
  dtostrf(humid, 5, 2, packetH);
  dtostrf(temp, 5, 2, packetT);
  strcpy(packet, packetID);
  strcat(packet, " ");
  strcat(packet, packetH);
  strcat(packet, " ");
  strcat(packet, packetT);
  strcat(packet, " ");

  // Actual packet sending
  rf95.send((uint8_t *)packet, 45);
  delay(10);

  // Confirm data was sent successfully
  rf95.waitPacketSent();
  sendConfirmQuiet();
}

// Function to check if data was received
void sendConfirm() {
  // Prepping variables
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);
 
  if (rf95.waitAvailableTimeout(1000)) { 
    // If reply is received   
    if (rf95.recv(buf, &len)) {
        display.print(" Got reply: ");
        display.println((char*)buf);
        display.print(" RSSI: ");
        display.println(rf95.lastRssi(), DEC);
        display.display();    
    }
    else {
        display.println(" Receive failed");
        display.display();
    }
  }
  else {
      display.println(" No reply");
      display.display();
  }
  delay(5000);
}

// Function to check if data was received without displaying it
void sendConfirmQuiet() {
  // Prepping variables
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);
 
  if (rf95.waitAvailableTimeout(1000)) { 
    // If reply is received   
    if (rf95.recv(buf, &len)) {
      // Add retries later 
    }
    else {
      // Add retries later
    }
  }
  else {
    // Add retries later
  }
}

void setup() {
  display.begin(0x3C, true); // Address 0x3C default

  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen internally, this will display the splashscreen.
  display.display();
  delay(1000);

  // Clear the buffer.
  display.clearDisplay();
  display.display();

  display.setRotation(1);

  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);

  // text display tests
  display.setTextSize(1);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(0,0);
  display.display(); // actually display all of the above

  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  delay(100);

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    display.println("LoRa radio init failed");
    display.println("Uncomment '#define SERIAL_DEBUG' in RH_RF95.cpp for detailed debug info");
    display.display();
    while (1);
  }

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    display.println("setFrequency failed");
    display.display();
    while (1);
  }
  
  rf95.setTxPower(23, false);

  display.println("Radio initialized");
  display.display();
  delay(500);

  if (!sensor.begin()) {
    display.println("Did not find Si7021 sensor!");
    display.display();
    while (true)
      ;
  }

  display.println("Sensor initialized");
  display.display();
  delay(500);
  
  display.println("Boot complete, all   tests successful");
  display.display();
  delay(3500);
  display.clearDisplay(); 
}

void loop() {
    
  display.clearDisplay();
  display.display();

  if(!digitalRead(BUTTON_A)) { 
    menu();
  }
  if(!digitalRead(BUTTON_B)) {
    menu();
  }
  if(!digitalRead(BUTTON_C)) {
    menu();
  }

  unsigned long now = millis();
  if (now -lastSampleTime >= minutes) {
    lastSampleTime += minutes;
    sendDataQuiet();
  }

}
