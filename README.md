# LoRaSensors

Code and guide for my LoRa Sensor Project

# Guide
## Parts
### Arduino
* [Main Board](https://www.adafruit.com/product/3178)
* [OLED Display](https://www.adafruit.com/product/4650)
* [Sensor](https://www.adafruit.com/product/3251)
* [Sensor Cable](https://www.adafruit.com/product/4210)
* [Board Header](https://www.adafruit.com/product/2886)
* Micro USB type B to USB type A cable

### Raspberry Pi
* [Main Board](https://www.adafruit.com/product/4292)
* [Radio Bonnet](https://www.adafruit.com/product/4074)
* [Power Supply](https://www.adafruit.com/product/4298)
* [Micro HDMI Cable](https://www.adafruit.com/product/2775)
* 32GB Micro SD Card with Raspberry Pi OS Lite

### Other
* Soldering Iron
* Solder Wire (I used rosin core solder but lead based will work as well)
* [Solid Core Wire](https://www.adafruit.com/product/290) (For Antenna)

## Radio Client Installation
### Assembly
* Solder the female headers to the Main Board [Guide](https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/assembly)
* Solder the male headers to the OLED [Guide](https://learn.adafruit.com/adafruit-128x64-oled-featherwing/assembly)
* Attach the sensor with the OLED with the QT/Qwiic JST cable

### Arduino IDE
* [IDE Setup - Board](https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/setup)
* [IDE Setup - OLED](https://learn.adafruit.com/adafruit-128x64-oled-featherwing/arduino-code)
* [IDE Setup - Sensor](https://learn.adafruit.com/adafruit-si7021-temperature-plus-humidity-sensor/arduino-code)
* Open the RadioClient.ino file
* Upload the file to your Arduino

## Radio Server Installation
### Pi Setup
* Plug in your radio bonnet to the Pi, it should overhang the main board
* Download and install Raspberry Pi OS to the Micro SD [Guide/Downloads](https://www.raspberrypi.org/software/)
* Boot the Raspberry Pi with the Micro SD
* Follow any setup instructions
* Start and enable SSH with `sudo systemctl start ssh && sudo systemctl enable ssh`
* Find the IP Adress with `ip a`
* Connect remotely via SSH [Guide](https://thehomeofthefuture.com/how-to/connect-to-a-computer-using-ssh/)

### Python Setup
* Install Python and CircutPython [Guide](https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi)
* Install bonnet software [Guide](https://learn.adafruit.com/adafruit-radio-bonnets/rfm9x-raspberry-pi-setup)
* Copy radioServer.py to the Raspberry Pi [Guide](https://stackabuse.com/using-scp-to-copy-and-securely-transfer-files-and-folders/)
* Run the program with `python3 radioServer.py`
