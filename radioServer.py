# Import Python System Libraries
import time
from datetime import date
import subprocess
# Import Blinka Libraries
import busio
from digitalio import DigitalInOut, Direction, Pull
import board
# Import the SSD1306 module.
import adafruit_ssd1306
# Import RFM9x
import adafruit_rfm9x

# Displays the most recently received packet
def lastMsg (packet_text):
    display.fill(0)
    display.text('Most recent message', 5, 0, 1)
    display.text('Got: ', 5, 9, 1)
    display.text(packet_text, 30, 9, 1)
    display.show()
    time.sleep(10)
    return

# Displays device information
def info():
    display.fill(0)
    cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
    CPU = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%s MB %.2f%%\", $3,$2,$3*100/$2 }'"
    MemUsage = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = 'df -h | awk \'$NF=="/"{printf "Disk: %d/%d GB  %s", $3,$2,$5}\''
    Disk = subprocess.check_output(cmd, shell=True).decode("utf-8")
    display.text(CPU, 0, 0, 1)
    display.text(MemUsage, 0, 9, 1)
    display.text(Disk, 0, 18, 1)
    display.show()
    time.sleep(10)
    return


# Menu used when listener is on
def listenerMenu (packet_text):
    time.sleep(0.5)
    display.fill(0)
    while True:
        display.text('Listener Menu', 20, 0, 1)
        display.text('A', 5, 12, 1)
        display.text('Last', 0, 24, 1)
        display.text('B', 53, 12, 1)
        display.text('Quit', 45, 24, 1)
        display.text('C', 101, 12, 1)
        display.text('Cancel', 86, 24, 1)
        display.show()
        
        # User input
        if not btnA.value:
            lastMsg(packet_text)
            time.sleep(0.5)
            display.fill(0)
        elif not btnB.value:
            # Causes listener to quit
            return True
        elif not btnC.value:
            # Keeps listener running
            return False

# Radio listener
def listener ():
    # Listener init
    packet_text = ''
    quit = False
    display.fill(0)
    display.text('Listener Started', 15, 0, 1)
    display.show()
    time.sleep(5)
    display.fill(0)
    display.show()
    
    # Main listener loop
    while True:
        packet = None
        display.fill(0)
        display.show()
        time.sleep(0.1) # Prevents 100% CPU Usage, remove for better responsiveness
        packet = rfm9x.receive()
        if packet is None:
            # Do nothing
            do = "nothing"
        else:
            # Try because if unreadable packet is received it would crash
            try:
                # Decodes the packet
                prev_packet = packet
                packet_text = str(prev_packet, "utf-8")
                pkt_dissct = packet_text.split(' ')
                packet_text = ''    #OLED text
                packet_csv = ''     #CSV text
                
                # Timestamps for saving to CSV
                d = date.today()
                d = d.strftime("%m/%d/%y")
                packet_csv += d
                packet_csv += ','
                t = time.localtime()
                t = time.strftime("%H:%M:%S", t)
                packet_csv += t
                packet_csv += ','
                
                # Grabs the first three values of the packet
                for i, val in enumerate(pkt_dissct):
                    if i == 0:      # Device ID
                        packet_csv += val
                        packet_csv += ','
                    elif i == 1:    # Humidity value
                        packet_text += val
                        packet_text += '% '
                        packet_csv += val
                        packet_csv += ','
                    elif i == 2:    # Temperature value
                        packet_text += val
                        packet_text += 'F'
                        packet_csv += val
                    else:           # Breaks after first two
                        break
                
                # Appends to CSV file db.csv
                csv = open("db.csv","a")
                csv.write(packet_csv)
                csv.write("\n")
                csv.close()
                
                # Prints messages for success and sends response
                #print(packet_csv) # Uncomment for CLI debugging
                display.text('Got: ', 5, 0, 1)
                display.text(packet_text, 30, 0, 1)
                response = bytes("Received","utf-8")
                rfm9x.send(response)
                display.text('Sent response', 5, 9, 1)
                display.show()
                time.sleep(5)
                
            except: # Catches malformed packets or radio noise
                print("Error in packet decoding")
                display.text('Error in packet', 0, 0, 1)
                display.show()
                time.sleep(5)
        
        # Brings up listener menu
        if not btnA.value:
            quit = listenerMenu(packet_text)
        elif not btnB.value:
            quit = listenerMenu(packet_text)
        elif not btnC.value:
            quit = listenerMenu(packet_text)
            
        if quit == True:
            break
        
    return 


# Menu when listener is off     
def mainMenu ():
    time.sleep(0.5)
    display.fill(0)
    while True:
        display.text('Main Menu', 29, 0, 1)
        display.text('A', 5, 12, 1)
        display.text('Info', 0, 24, 1)
        display.text('B', 53, 12, 1)
        display.text('Start', 43, 24, 1)
        display.text('C', 101, 12, 1)
        display.text('Cancel', 86, 24, 1)
        display.show()
        
        # User input
        if not btnA.value:
            info()
            display.fill(0)
            time.sleep(0.5)
        elif not btnB.value:
            listener()
            display.fill(0)
            time.sleep(0.5)
            break
        elif not btnC.value:
            display.fill(0)
            break
    
    return

# Main Program

# Button A
btnA = DigitalInOut(board.D5)
btnA.direction = Direction.INPUT
btnA.pull = Pull.UP

# Button B
btnB = DigitalInOut(board.D6)
btnB.direction = Direction.INPUT
btnB.pull = Pull.UP

# Button C
btnC = DigitalInOut(board.D12)
btnC.direction = Direction.INPUT
btnC.pull = Pull.UP

# Create the I2C interface.
i2c = busio.I2C(board.SCL, board.SDA)

# 128x32 OLED Display
reset_pin = DigitalInOut(board.D4)
display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c, reset=reset_pin)
width = display.width
height = display.height
display.text('Booting...', 0, 0, 1)
display.text('Display Initialized', 0, 8, 1)
display.show()
time.sleep(0.75)

# Configure LoRa Radio
CS = DigitalInOut(board.CE1)
RESET = DigitalInOut(board.D25)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, 915.0)
rfm9x.tx_power = 23
prev_packet = None
display.text('Radio Initialized', 0, 16, 1)
display.show()
time.sleep(0.75)

display.text('Boot complete', 0, 24, 1)
display.show()
time.sleep(2)

# Helper text
display.fill(0)
display.text('Press any button to', 0, 0, 1)
display.text('launch menu', 0, 9, 1)
display.show()
time.sleep(3)

# Main loop
while True:    
    
    if not btnA.value:
        mainMenu()
    elif not btnB.value:
        mainMenu()
    elif not btnC.value:
        mainMenu()
        
    display.fill(0)
    display.show()
    time.sleep(0.1)

